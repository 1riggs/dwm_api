from setuptools import setup, find_packages

setup(
    version='0.0.1',
    name='pytamer',
    packages=find_packages(),
    requirements=[
        'networkx',
        'requests'
    ]
)