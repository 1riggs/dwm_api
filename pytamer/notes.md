trying to find the "minimal breeding tree" for a monster.

every monster has a fuckload of ways you could go about creating it. so i want to know what's the quickest way. the 
quickest way is the one with the fewest number of monsters i have to catch or breed to get the desired result.

bfs
beam search
beam stack search
dfs
depth first beam search
beam search using limited discrepancy backtracking
divide and conquer with dfs beam search


minimize height === minimize vertices === minimize total leaves

each gen contains at most 2^n, where n is height of tree (tree with one vertex is height 0)

