import queue
from typing import List

import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout

from .request import get_lineage_vertices
from .vertex import Vertex


def _get_user_lineage_selection(vertices: List[Vertex]) -> Vertex:
    if len(vertices) == 1:
        return vertices[0]
    for idx, node in enumerate(vertices):
        print(
            f'{idx}) {node.offspring}, '
            f'{node.pedigree if node.pedigree else node.pedigree_family}-'
            f'{node.mate if node.mate else node.mate_family}, w={node.weight} '
        )
    selected_idx = -1
    while selected_idx not in list(range(len(vertices))):
        selected_idx = int(input('Enter number to select lineage: '))
    return vertices[selected_idx]


def build_user_specified_graph() -> (Vertex, nx.DiGraph):
    graph = nx.DiGraph()
    q = queue.SimpleQueue()

    monster = input('enter monster: ')
    nodes = get_lineage_vertices(monster)
    if not nodes:
        print('Monster not found, exiting...')
        return None, None
    root = _get_user_lineage_selection(nodes)
    q.put(root)

    while not q.empty():
        node = q.get()
        graph.add_node(node)

        pedigree: str = node.pedigree if node.pedigree else node.pedigree_family
        mate: str = node.mate if node.mate else node.mate_family

        nodes = get_lineage_vertices(pedigree)
        if nodes:
            selection = _get_user_lineage_selection(nodes)
            graph.add_edge(node, selection)
            q.put(selection)
        else:
            leaf = Vertex(node.pedigree_family)
            graph.add_node(leaf)
            graph.add_edge(node, leaf)

        nodes = get_lineage_vertices(mate)
        if nodes:
            selection = _get_user_lineage_selection(nodes)
            graph.add_edge(node, selection)
            q.put(selection)
        else:
            leaf = Vertex(node.mate_family)
            graph.add_node(leaf)
            graph.add_edge(node, leaf)

    return root, graph


def draw_graph_as_tree(graph, root):
    bfs = nx.bfs_tree(graph, root)
    r_bfs = bfs.reverse()
    layout = graphviz_layout(r_bfs, prog='dot')
    nx.draw(r_bfs, layout, with_labels=True, node_size=5000)


def draw_graph(graph):
    nx.draw(graph, with_labels=True, node_size=5000)


if __name__ == '__main__':
    r, g = build_user_specified_graph()
    if r and g:
        draw_as_tree = input('Draw resulting graph using graphviz dot layout? (y/n)')
        if draw_as_tree.lower() == 'y':
            draw_graph_as_tree(g, r)
        draw = input('Draw resulting graph without layout? (y/n)')
        if draw.lower() == 'y':
            draw_graph(g.reverse())
