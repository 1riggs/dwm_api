import typing

import requests

from .config import settings
from .vertex import Vertex


def get_monster_lineage(monster_id: str) -> typing.List:
    r = requests.get(f'{settings.API_ENDPOINT}monsters/{monster_id}/lineage')
    if r.ok:
        return r.json()
    else:
        r.raise_for_status()
        return []


def get_lineage_vertices(monster_id: str) -> typing.List[Vertex]:
    return [
        Vertex(
            lineage['id_offspring'],
            lineage['id_pedigree'],
            lineage['id_mate'],
            lineage['id_pedigree_family'].upper(),
            lineage['id_mate_family'].upper()
        ) for lineage in get_monster_lineage(monster_id)
    ]


class APIRequest:

    @staticmethod
    def get_monster_lineage(monster_id: str) -> typing.List:
        r = requests.get(f'{settings.API_ENDPOINT}monsters/{monster_id}/lineage')
        if r.ok:
            return r.json()
        else:
            r.raise_for_status()
            return []

    def get_lineage_vertices(self, monster_id: str) -> typing.List[Vertex]:
        return [
            Vertex(
                lineage['id_offspring'],
                lineage['id_pedigree'],
                lineage['id_mate'],
                lineage['id_pedigree_family'].upper(),
                lineage['id_mate_family'].upper()
            ) for lineage in self.get_monster_lineage(monster_id)
        ]


request = APIRequest()
