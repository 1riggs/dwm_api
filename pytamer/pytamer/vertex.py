import uuid


class Vertex:
    def __init__(self, offspring, pedigree=None, mate=None, pedigree_family=None, mate_family=None):
        self.offspring = offspring
        self.pedigree = pedigree
        self.mate = mate
        self.pedigree_family = pedigree_family
        self.mate_family = mate_family
        self.uuid = uuid.uuid4()
        self.weight = self.get_weight()

    def get_weight(self):
        total = 0
        if self.pedigree is not None:
            total += 1
        if self.mate is not None:
            total += 1
        return total

    def __repr__(self):
        # return f'{self.offspring}: {self.pedigree}({self.pedigree_family})-{self.mate}({self.mate_family})'
        return f'{self.uuid}'

    def __format__(self, format_spec):
        """
        https://stackoverflow.com/questions/28904602/what-is-the-pythonic-way-to-implement-a-str-method-with-different-format-opt
        :param format_spec:
        :return:
        """
        return str(self).__format__(format_spec)

    def __hash__(self):
        return hash(self.uuid)

    def __eq__(self, other):
        if isinstance(other, Vertex):
            return self.uuid == other.uuid
        else:
            return False

    # def __ne__(self, other):
    #     return str(self) != str(other)
    #
    # def __lt__(self, other):
    #     return str(self) < str(other)
    #
    # def __le__(self, other):
    #     return str(self) <= str(other)
    #
    # def __gt__(self, other):
    #     return str(self) > str(other)
    #
    # def __ge__(self, other):
    #     return str(self) >= str(other)

