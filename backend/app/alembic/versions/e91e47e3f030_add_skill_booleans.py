"""add skill booleans

Revision ID: e91e47e3f030
Revises: 801801fd4b9e
Create Date: 2020-10-21 23:56:31.948559

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'e91e47e3f030'
down_revision = '801801fd4b9e'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('skills', sa.Column('is_progressive', sa.Boolean, nullable=False))
    op.add_column('skills', sa.Column('is_combo_result', sa.Boolean, nullable=False))
    op.add_column('skills', sa.Column('is_combo_part', sa.Boolean, nullable=False))


def downgrade():
    op.drop_column('skills', 'is_combo_part')
    op.drop_column('skills', 'is_combo_result')
    op.drop_column('skills', 'is_progressive')
