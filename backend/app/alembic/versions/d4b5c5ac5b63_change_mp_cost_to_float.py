"""change mp cost to float

Revision ID: d4b5c5ac5b63
Revises: e91e47e3f030
Create Date: 2020-10-22 00:48:36.739868

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'd4b5c5ac5b63'
down_revision = 'e91e47e3f030'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('skills', 'mp_cost', type_=sa.Float, postgresql_using='mp_cost::real')


def downgrade():
    op.alter_column('skills', 'mp_cost', type_=sa.Integer, postgresql_using='mp_cost::int')
