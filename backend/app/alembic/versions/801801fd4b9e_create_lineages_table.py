"""create lineages table

Revision ID: 801801fd4b9e
Revises: ba1871a6134b
Create Date: 2020-10-19 23:54:23.667986

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '801801fd4b9e'
down_revision = 'ba1871a6134b'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'lineages',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('id_offspring', sa.Text, sa.ForeignKey('monsters.id')),
        sa.Column('id_pedigree', sa.Text, nullable=True),
        sa.Column('id_mate', sa.Text, nullable=True),
        sa.Column('id_pedigree_family', sa.Text, sa.ForeignKey('monster_families.id')),
        sa.Column('id_mate_family', sa.Text, sa.ForeignKey('monster_families.id')),
        sa.Column('note', sa.Text)
    )


def downgrade():
    op.drop_table('lineages')
