"""Add skill detail tables

Revision ID: bb6c02ecee5d
Revises: d4b5c5ac5b63
Create Date: 2020-10-25 19:44:11.561893

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'bb6c02ecee5d'
down_revision = 'd4b5c5ac5b63'
branch_labels = None
depends_on = None


def upgrade():
    # skill requirements
    op.create_table(
        'skill_requirements',
        sa.Column('id', sa.Text, sa.ForeignKey('skills.id'), primary_key=True),
        sa.Column('level', sa.Integer),
        sa.Column('hp', sa.Integer),
        sa.Column('mp', sa.Integer),
        sa.Column('attack', sa.Integer),
        sa.Column('defense', sa.Integer),
        sa.Column('intelligence', sa.Integer),
        sa.Column('agility', sa.Integer)
    )

    # skill progressions
    op.create_table(
        'skill_progressions',
        sa.Column('id', sa.Text, sa.ForeignKey('skills.id'), primary_key=True),
        sa.Column('stage1', sa.Text),
        sa.Column('stage2', sa.Text),
        sa.Column('stage3', sa.Text),
        sa.Column('stage4', sa.Text)
    )

    # skill combinations
    op.create_table(
        'skill_combinations',
        sa.Column('id', sa.Text, sa.ForeignKey('skills.id'), primary_key=True),
        sa.Column('part1', sa.Text),
        sa.Column('part2', sa.Text),
        sa.Column('part3', sa.Text),
        sa.Column('part4', sa.Text),
        sa.Column('part5', sa.Text)
    )


def downgrade():
    op.drop_table('skill_combinations')
    op.drop_table('skill_progressions')
    op.drop_table('skill_requirements')
