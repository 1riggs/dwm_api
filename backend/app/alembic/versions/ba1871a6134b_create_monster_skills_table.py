"""create monster skills table

Revision ID: ba1871a6134b
Revises: 7978ee91c808
Create Date: 2020-10-19 23:20:25.466644

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ba1871a6134b'
down_revision = '7978ee91c808'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'monster_skills',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('id_monster', sa.Text, sa.ForeignKey('monsters.id'), unique=True, nullable=False),
        sa.Column('id_skill1', sa.Text, sa.ForeignKey('skills.id')),
        sa.Column('id_skill2', sa.Text, sa.ForeignKey('skills.id')),
        sa.Column('id_skill3', sa.Text, sa.ForeignKey('skills.id')),
    )


def downgrade():
    op.drop_table('monster_skills')
