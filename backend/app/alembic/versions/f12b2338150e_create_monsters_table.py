"""create monsters table

Revision ID: f12b2338150e
Revises: a0b955b44bb5
Create Date: 2020-10-19 22:47:49.650617

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'f12b2338150e'
down_revision = 'a0b955b44bb5'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'monsters',
        sa.Column('id', sa.Text, primary_key=True, comment='monster name cast to lowercase'),
        sa.Column('id_monster_family', sa.Text, sa.ForeignKey('monster_families.id')),
        sa.Column('name', sa.Text, unique=True, comment='proper monster name'),
        sa.Column('max_level', sa.Integer),
        sa.Column('sex_probability', sa.Text),
        sa.Column('is_flying', sa.Boolean),
        sa.Column('is_metal', sa.Boolean)
    )


def downgrade():
    op.drop_table('monsters')
