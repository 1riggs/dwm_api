"""create_monster_families_table

Revision ID: a0b955b44bb5
Revises: 
Create Date: 2020-10-19 22:04:22.416702

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'a0b955b44bb5'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "monster_families",
        sa.Column('id', sa.Text, primary_key=True, comment='monster family name cast to lowercase'),
        sa.Column('name', sa.Text, nullable=False, unique=True, comment='monster family in proper casing')
    )


def downgrade():
    op.drop_table('monster_families')
