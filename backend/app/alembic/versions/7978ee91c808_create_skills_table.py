"""create skills table

Revision ID: 7978ee91c808
Revises: f12b2338150e
Create Date: 2020-10-19 23:15:46.996833

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7978ee91c808'
down_revision = 'f12b2338150e'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'skills',
        sa.Column('id', sa.Text, primary_key=True, comment='skill name cast to lowercase'),
        sa.Column('name', sa.Text, unique=True, comment='skill proper name'),
        sa.Column('mp_cost', sa.Integer),
        sa.Column('description', sa.Text)
    )


def downgrade():
    op.drop_table('skills')
