#! /usr/bin/env bash

# Let the DB start
sleep 10;
# Run migrations
alembic upgrade head
# Load data into database
python /app/app/seed_data.py
