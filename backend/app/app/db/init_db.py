import logging

import pandas as pd
from app.models.lineage import Lineage
from app.models.monster import Monster
from app.models.monster_family import MonsterFamily
from app.models.skill import Skill
from app.models.skill_combination import SkillCombination
from app.models.skill_progression import SkillProgression
from app.models.skill_requirement import SkillRequirement
from sqlalchemy.orm import Session

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def _seed_table_from_csv(session: Session, csv_path: str, model):
    df = pd.read_csv(csv_path)
    df = df.mask(df.isna(), other=None)
    to_add = []
    for _, row in df.iterrows():
        db_obj = model(**row)
        to_add.append(db_obj)
    session.add_all(to_add)
    session.commit()


def init_db(session: Session) -> None:
    if not session.execute('select id from monster_families limit 1').fetchall():
        logging.info('Seeding monster_families table...')
        _seed_table_from_csv(session, '/data/seed_files/monster_families.csv', MonsterFamily)

    if not session.execute('select id from monsters limit 1').fetchall():
        logging.info('Seeding monsters table...')
        _seed_table_from_csv(session, '/data/seed_files/monsters.csv', Monster)

    if not session.execute('select id from lineages limit 1').fetchall():
        logging.info('Seeding lineages table...')
        _seed_table_from_csv(session, '/data/seed_files/lineages.csv', Lineage)

    if not session.execute('select id from skills limit 1').fetchall():
        logging.info('Seeding skills table...')
        _seed_table_from_csv(session, '/data/seed_files/skills.csv', Skill)

    if not session.execute('select id from skill_requirements limit 1').fetchall():
        logging.info('Seeding skill_requirements table...')
        _seed_table_from_csv(session, '/data/seed_files/skill_requirements.csv', SkillRequirement)

    if not session.execute('select id from skill_progressions limit 1').fetchall():
        logging.info('Seeding skill_progressions table...')
        _seed_table_from_csv(session, '/data/seed_files/skill_progressions.csv', SkillProgression)

    if not session.execute('select id from skill_combinations limit 1').fetchall():
        logging.info('Seeding skill_combinations table...')
        _seed_table_from_csv(session, '/data/seed_files/skill_combinations.csv', SkillCombination)