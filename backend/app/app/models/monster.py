from app.db.base_class import Base
from sqlalchemy import Integer, Text, Column, ForeignKey, Boolean


class Monster(Base):
    __tablename__ = 'monsters'

    id = Column(Text, primary_key=True, comment='monster name cast to lowercase')
    id_monster_family = Column(Text, ForeignKey('monster_families.id'))
    name = Column(Text, unique=True, comment='proper monster name')
    max_level = Column(Integer)
    sex_probability = Column(Text)
    is_flying = Column(Boolean)
    is_metal = Column(Boolean)



