from app.db.base_class import Base
from sqlalchemy import Column, ForeignKey, Text, Integer


class SkillRequirement(Base):
    __tablename__ = 'skill_requirements'

    id = Column(Text, ForeignKey('skills.id'), primary_key=True)
    level = Column(Integer)
    hp = Column(Integer)
    mp = Column(Integer)
    attack = Column(Integer)
    defense = Column(Integer)
    intelligence = Column(Integer)
    agility = Column(Integer)

