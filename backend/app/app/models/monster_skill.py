from app.db.base_class import Base
from sqlalchemy import Integer, Column, Text, ForeignKey


class MonsterSkill(Base):
    __tablename__ = 'monster_skills'

    id = Column(Integer, primary_key=True)
    id_monster = Column(Text, ForeignKey('monsters.id'), unique=True, nullable=False)
    id_skill1 = Column(Text, ForeignKey('skills.id'))
    id_skill2 = Column(Text, ForeignKey('skills.id'))
    id_skill3 = Column(Text, ForeignKey('skills.id'))
