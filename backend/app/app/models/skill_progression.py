from app.db.base_class import Base
from sqlalchemy import Column, ForeignKey, Text


class SkillProgression(Base):
    __tablename__ = 'skill_progressions'

    id = Column(Text, ForeignKey('skills.id'), primary_key=True)
    stage1 = Column(Text)
    stage2 = Column(Text)
    stage3 = Column(Text)
    stage4 = Column(Text)
