from app.db.base_class import Base
from sqlalchemy import Column, ForeignKey, Text


class SkillCombination(Base):
    __tablename__ = 'skill_combinations'

    id = Column(Text, ForeignKey('skills.id'), primary_key=True)
    part1 = Column(Text)
    part2 = Column(Text)
    part3 = Column(Text)
    part4 = Column(Text)
    part5 = Column(Text)
