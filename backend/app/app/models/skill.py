from app.db.base_class import Base
from sqlalchemy import Text, Integer, Column, Boolean


class Skill(Base):
    __tablename__ = 'skills'

    id = Column(Text, primary_key=True, comment='skill name cast to lowercase')
    name = Column(Text, unique=True, comment='skill proper name')
    mp_cost = Column(Integer)
    description = Column(Text)
    is_progressive = Column(Boolean, nullable=False)
    is_combo_result = Column(Boolean, nullable=False)
    is_combo_part = Column(Boolean, nullable=False)
