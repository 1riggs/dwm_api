from app.db.base_class import Base
from sqlalchemy import Text, Integer, Column, ForeignKey


class Lineage(Base):
    __tablename__ = 'lineages'

    id = Column(Integer, primary_key=True)
    id_offspring = Column(Text, ForeignKey('monsters.id'))
    id_pedigree = Column(Text, nullable=True)
    id_mate = Column(Text, nullable=True)
    id_pedigree_family = Column(Text, ForeignKey('monster_families.id'))
    id_mate_family = Column(Text, ForeignKey('monster_families.id'))
    note = Column(Text)
