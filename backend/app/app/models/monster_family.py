from app.db.base_class import Base
from sqlalchemy import Text, Column


class MonsterFamily(Base):
    __tablename__ = 'monster_families'

    id = Column(Text, primary_key=True)
    name = Column(Text, unique=True, nullable=False)
