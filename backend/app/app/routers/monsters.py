from typing import Any

from app import query
from app import utils
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

router = APIRouter()


@router.get('/')
def read_monsters(
        session: Session = Depends(utils.get_session),
        skip: int = 0,
        limit: int = 20,
) -> Any:
    return query.monster.get_all(session, skip=skip, limit=limit)


@router.get('/{id_monster}')
def read_monster(
        *,
        session: Session = Depends(utils.get_session),
        id_monster: str
) -> Any:
    monster = query.monster.get(session, id_monster)
    if not monster:
        raise HTTPException(status_code=404, detail=f'Monster, {id_monster}, not found')
    return monster


@router.get('/{id_monster}/lineage')
def read_monster_lineage(
        *,
        session: Session = Depends(utils.get_session),
        id_monster: str
) -> Any:
    return query.lineage.get_lineages(session, id_monster)

@router.get('/{id_monster}/skills')
def read_monster_skill_set(
    *,
    session: Session = Depends(utils.get_session),
    id_monster: str
) -> Any:
    return query.monster_skill.get_monster_skill_set(session, id_monster)
