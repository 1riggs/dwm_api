from typing import Any

from app import query
from app import utils
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

router = APIRouter()


@router.get('/')
def read_skills(
    session: Session = Depends(utils.get_session),
    skip: int = 0,
    limit: int = 20,
) -> Any:
    return query.skill.get_all(session, skip=skip, limit=limit)


@router.get('/{id_skill}')
def read_skill(
    *,
    session: Session = Depends(utils.get_session),
    id_skill: str
) -> Any:
    return query.skill.get(session, id_skill)
