from app.models.monster_skill import MonsterSkill

from .base import QueryBase

class MonsterSkillQuery(QueryBase):
    def __init__(self):
        super().__init__(MonsterSkill)

    def get_monster_skill_set(self, session, id_monster):
        return session.query(self.model).filter(self.model.id_monster == id_monster).one()

monster_skill = MonsterSkillQuery()