from app.models.monster import Monster
from app.query.base import QueryBase


class MonsterQuery(QueryBase):
    def __init__(self):
        super().__init__(Monster)


monster = MonsterQuery()
