from app.models.lineage import Lineage

from .base import QueryBase


class LineageQuery(QueryBase):
    def __init__(self):
        super().__init__(Lineage)

    def get_lineages(self, session, id_offspring):
        return session.query(self.model).filter(self.model.id_offspring == id_offspring).all()


lineage = LineageQuery()
