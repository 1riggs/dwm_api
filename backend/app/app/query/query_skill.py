from app.models.skill import Skill

from .base import QueryBase

class SkillQuery(QueryBase):
    def __init__(self):
        super().__init__(Skill)

skill = SkillQuery()