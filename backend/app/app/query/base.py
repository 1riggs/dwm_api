from typing import TypeVar, Type, Any, List

from app.db.base_class import Base
from sqlalchemy.orm import Session

ModelType = TypeVar("ModelType", bound=Base)


class QueryBase:
    def __init__(self, model: Type[ModelType]):
        self.model = model

    def get(self, session: Session, _id: Any):
        return session.query(self.model).filter(self.model.id == _id).first()

    def get_all(self, session: Session, *, skip: int = 0, limit: int = 20) -> List[ModelType]:
        if limit is None:
            return session.query(self.model).offset(skip).all()
        else:
            return session.query(self.model).offset(skip).limit(limit).all()
