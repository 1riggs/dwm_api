from app.routers import monsters
from app.routers import skills
from fastapi import FastAPI
from app.core.config import settings

from starlette.middleware.cors import CORSMiddleware

app = FastAPI(
    title='DWM-API', description="An API designed to aid players of Dragon Warrior Monsters (GBC)"
)

if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"]
    )

@app.get("/")
def read_root():
    return {"message": "Welcome to an API designed to aid players of Dragon Warrior Monsters (GBC)"}


app.include_router(monsters.router, prefix='/monsters', tags=['Monsters'])
app.include_router(skills.router, prefix='/skills', tags=['Skills'])