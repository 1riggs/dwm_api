from setuptools import setup, find_namespace_packages, find_packages

setup(
    name="app",
    version="0.0.1",
    packages=find_packages(include='app/*'),
    setup_requires=[
        'setuptools',
        'wheel'
    ],
    install_requires=[
        'pandas',
        'alembic',
        'psycopg2-binary'
    ]
)
