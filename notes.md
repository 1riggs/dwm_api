### sources
* http://tartarus.rpgclassics.com/archive/staff/dwm/magic.shtml
* https://www.spriters-resource.com/game_boy_gbc/dwmons/

### similar projects
* http://www.monster-db.com/
* https://darksair.org/dwm2-breed/

## ideas

? the min height tree for any node is comprised of the minimum height tree for its pedigree and mate nodes

adding weights

* nodes
    * weight determined by pedigree and mate +1 if pedigree is not none, +1 if mate is not none
        * => Weight(Node(dracolord1: servant-andreal)) = 2
        * => Weight(Node(andreal: DRAGON-gulpple)) = 1
        * => Weight(Node(amberweed: PLANT-MATERIAL)) = 0
    * Weight(Tree) = sum of all of its nodes
* edges
    * weight is determined by  edge type, if it connects to a monster node +1 if it connects to a family node +0

Using node weights in networkx
* take the average of start and end node weights of an edge and add it to the weight of the edge.
