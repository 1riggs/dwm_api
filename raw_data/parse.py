import pandas as pd
import re


def parse_lineages():
    m_df = pd.read_csv('backend/data/monsters.csv')
    in_df = pd.read_csv('raw_data/lineages.csv')
    in_df = in_df.dropna(subset=['PARENT1', 'PARENT2'])
    in_df['PARENT1'] = in_df['PARENT1'].str.split(' ')
    in_df['PARENT2'] = in_df['PARENT2'].str.split(' ')

    in_df = in_df.explode('PARENT1')
    in_df = in_df.explode('PARENT2')

    out_df = pd.DataFrame()
    out_df['id_offspring'] = in_df['NAME'].str.lower()
    out_df['id_pedigree'] = in_df['PARENT1'].apply(lambda x: None if x.startswith('*') else x.lower())
    out_df['id_mate'] = in_df['PARENT2'].apply(lambda x: None if x.startswith('*') else x.lower())
    out_df['id_pedigree_family'] = in_df['PARENT1'].apply(lambda x: x.strip('*').lower() if x.startswith('*') else None)
    out_df['id_mate_family'] = in_df['PARENT2'].apply(lambda x: x.strip('*').lower() if x.startswith('*') else None)

    for monster in m_df[['id', 'id_monster_family']].itertuples():
        out_df.loc[out_df['id_pedigree'] == monster.id, 'id_pedigree_family'] = monster.id_monster_family
        out_df.loc[out_df['id_mate'] == monster.id, 'id_mate_family'] = monster.id_monster_family

    out_df['note'] = in_df['COMMENTS']


def parse_skills():
    in_df = pd.read_csv('raw_data/skills.csv')

    skills = pd.DataFrame()
    skills['id'] = in_df['SkillName'].str.lower()
    skills['name'] = in_df['SkillName']
    skills['mp_cost'] = in_df['MPCost']
    skills['description'] = in_df['Description']
    skills['is_progressive'] = in_df['SkillStage1'].isna() == False
    skills['is_combo_result'] = in_df['ComboResult']
    skills['is_combo_part'] = in_df['ComboPart']

    req_df = pd.DataFrame()
    req_df['id'] = in_df['SkillName'].str.lower()
    req_df['level'] = in_df['ReqLevel']
    req_df['hp'] = in_df['ReqHP']
    req_df['mp'] = in_df['ReqMP']
    req_df['attack'] = in_df['ReqATK']
    req_df['defense'] = in_df['ReqDEF']
    req_df['intelligence'] = in_df['ReqINT']
    req_df['agility'] = in_df['ReqAGL']

    prog_df = pd.DataFrame()
    prog_df['id'] = in_df['SkillName'].str.lower()

    for col in in_df.columns:
        if 'SkillStage' in col:
            prog_df[re.sub(r'.*(\d)', r'stage\g<1>', col)] = in_df[col]
    prog_df = prog_df.dropna(subset=['stage1', 'stage2', 'stage3', 'stage4'], how='all')

    combo_df = pd.DataFrame()
    combo_df['id'] = in_df['SkillName'].str.lower()

    for col in in_df.columns:
        if re.search(r'ComboPart\d', col):
            combo_df[re.sub(r'.*(\d)', r'part\g<1>', col)] = in_df[col]

    combo_df = combo_df.dropna(subset=['part1', 'part2', 'part3', 'part4', 'part5'], how='all')
