 R E S I S T A N C E
---------------------

The following 26 classes of special attack exist in DWM:

A: Blaze (etc.), FireSlash, BigBang, FireStaff
B: Firebal (etc.), LavaStaff
C: Bang (etc.)
D: Infernos (etc.), VacuSlash, MultiCut, WindBeast, Vacuum, WindStaff
E: Bolt (etc.), BoltSlash, Lightning, Hellblast, BoltStaff
F: IceBolt (etc.), IceSlash, SnowStaff
G: Surround, SandStorm, Radiant
H: Sleep, NapAttack, SleepAir
I: Beat (etc.), K.O.Dance, EerieLite, UltraDown
J: RobMagic (etc.), OddDance, RobDance
K: StopSpell, MistStaff
L: PanicAll, PaniDance
M: Sap, SickLick (DEF down)
N: Slow (AGL down)
O: Sacrifice, Ramming, Kamikaze
P: MegaMagic
Q: FireAir (etc.)
R: IceAir (etc.)
S: PoisonHit, PoisonGas, PoisonAir, BadMeat
T: Paralyze, PalsyAir
U: Curse
V: Ahhh, LureDance, LushLicks, LegSweep, BigTrip, WarCry (Miss-a-Turn)
W: DanceShut
X: MouthShut
Y: CallHelp, YellHelp, RockThrow
Z: GigaSlash

Resistance to these attacks is a species characteristic and
ranges from 0 to 3, 0 being completely non-resistant and 3
being (almost) completely immune.

When a monster is bred, it has a chance to inherit resistances
from its parents that are higher than the norm for its species.
The chance of inheriting an exceptional resistance is related
to the new monster's "plus" value.  You can evaluate an egg to
find out whether the monster inherited an exceptional
resistance or not, but not what exceptional resistance was
inherited.